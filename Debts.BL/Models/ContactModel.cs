﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts.BL.Models
{
    public class ContactModel : BaseModel
    {
        public string Name { get; set; }

        public string Phone { get; set; }

        public double Balance { get; set; }

        public double Floor { get; set; }

        public DateTime? Reminder { get; set; }

        public string Description { get; set; }

        public List<PaymentModel> Payments { get; set; }

        public ContactModel()
        {
            Payments = new List<PaymentModel>();
        }
    }
}
