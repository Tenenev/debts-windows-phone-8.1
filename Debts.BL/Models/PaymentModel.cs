﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts.BL.Models
{
    public class PaymentModel : BaseModel
    {
        public DateTime DateTime { get; set; }

        public double Amount { get; set; }
    }
}
