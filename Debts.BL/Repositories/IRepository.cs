﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Debts.BL.Models;

namespace Debts.BL.Repositories
{
    public interface IRepository
    {
    }

    public interface IRepository<T> : IRepository where T : BaseModel
    {
        IQueryable<T> GetAll();

        T GetById(Guid id);

        void Add(T model);

        void Update(T model);

        void Delete(T model);

        void SaveChanges();
    }
}
