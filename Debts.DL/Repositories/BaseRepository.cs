﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Debts.BL.Models;
using Debts.BL.Repositories;

namespace Debts.DL.Repositories
{
    public abstract class BaseRepository : IRepository
    {
    }

    public abstract class BaseRepository<T> : IRepository<T> where T : BaseModel
    {
        protected readonly IRepositoryContext Context;

        public BaseRepository(IRepositoryContext context)
        {
            this.Context = context;
        }

        public IQueryable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Add(T model)
        {
            throw new NotImplementedException();
        }

        public void Update(T model)
        {
            throw new NotImplementedException();
        }

        public void Delete(T model)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }
}
